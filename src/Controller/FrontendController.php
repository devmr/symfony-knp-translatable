<?php

namespace App\Controller;

 
use App\Entity\Product;
use App\Entity\ProductTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontendController extends AbstractController
{
    /**
     * @Route("/", name="frontend")
     */
    public function index()
    { 

        $entityManager = $this->getDoctrine()->getManager();

   
        $product = new Product();
        $product->translate('fr')->setName('Chaussure Nike de bonne qualité');
        $product->translate('en')->setName('Nike Shoes better quality');
        $product->translate('fr')->setDescription('Pour sportifs courreurs');
        $product->translate('en')->setDescription('For running activity');

        $product->setCreatedAt(new \DateTime('now'));
        $product->setUser('Admin');

        $entityManager->persist($product);

        // In order to persist new translations, call mergeNewTranslations method, before flush
        $product->mergeNewTranslations();

        $entityManager->flush();

        return $this->redirectToRoute('frontend_list');
    }

    /**
     * @Route({
     *  "en": "/list",
     *  "fr": "/fr/list"
     * } , name="frontend_list")
     */
    public function list()
    { 

        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->render('frontend/index.html.twig', [
            'products' => $products,
        ]);
    }
}
